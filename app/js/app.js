(function ($) {
	
	$(function() { // document.ready
		
		var $html = $("html");
		var $body = $("body");
		
		// lazy load init
		$(".lazy-load").lazy({
			attribute: "data-lazy-src",
			threshold: 0,
			afterLoad: function(element) {
				element.addClass("loaded");
			},
		});
		
		// input mask
		var dateMaskConfig = {
			mask: "1.2.y",
			leapday: "29-02-",
			separator: ".",
			alias: "dd.mm.yyyy",
			placeholder: "ДД.ММ.ГГГГ",
			showMaskOnHover: false
		};
		$(".date-mask").inputmask("dd.mm.yyyy", dateMaskConfig);
		$(".phone-mask").inputmask({
			"mask": "+9 (999) 999-9999",
			showMaskOnHover: false
		});
		
		// autosize textarea
		autosize($(".autosize-textarea"));
		
		// regular select
		var $regularSelect = $(".regular-select");
		var $resetThisSelect = $(".reset-this-select");
		$regularSelect.each(function () {
			var $this = $(this);
			var placeholder = $this.attr("placeholder");
			var searchExist = $this.attr("data-search");
			var searchText = $this.attr("data-search-text");
			if (searchExist === "false") {
				searchExist = false;
			} else {
				searchExist = true;
			}
			$this.SumoSelect({
				csvDispCount: 3,
				placeholder: placeholder,
				search: searchExist,
				searchText: searchText,
				noMatch: 'Нет совпадений для "{0}"',
				captionFormatAllSelected: "",
				okCancelInMulti: false,
				locale: ['Применить', 'Сбросить', 'Выбраны все']
			});
			$this.on("change", function(){
				$this.closest(".regular-select-box").find($resetThisSelect).addClass("visible");
			})
		});
		$resetThisSelect.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.parent().find("select").prop('selectedIndex',0);
			$this.closest(".regular-select-box").find(".regular-select")[0].sumo.reload();
			$this.removeClass("visible");
		});
		
		// regular tabs
		var $regularTabsLinks = $(".regular-tabs-links");
		var $regularTabsLink = $regularTabsLinks.find("a");
		var $regularTabsContent = $(".regular-tabs-content");
		$regularTabsLink.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			var thisContentID = $this.data("tab");
			$regularTabsLink.removeClass("active");
			$this.addClass("active");
			$regularTabsContent.removeClass("active");
			$(thisContentID).addClass("active");
			if($html.hasClass("mobile")){
				setTimeout(function() {
					$("html, body").animate({
						scrollTop: $(thisContentID).offset().top - 15
					}, 200);
					return false;
				}, 200);
			}
		});
		
		// float label in inputs
		var $inputBoxWithFloat = $(".input-box.with-float-label");
		var $inputWithFloat = $inputBoxWithFloat.find("input, textarea");
		$inputWithFloat.each(function() {
			if (!($(this).val() === "")) {
				$(this).parent().addClass("in-focus");
			}
		});
		$inputWithFloat.focus(function(){
			$(this).parent().addClass("in-focus");
			
		}).blur(function(){
			if (!($(this).val() === "")) {
				$(this).parent().addClass("in-focus");
			} else {
				$(this).parent().removeClass("in-focus");
			}
		});
		
		// modal windows
		$("[data-fancybox]").fancybox({
			idleTime: false
		});
		var $modalOpener = $(".open-modal");
		$modalOpener.fancybox({
			animationEffect: "fade",
			touch: false,
			autoFocus: false,
			baseClass: "regular-modal-fancybox",
			i18n: {
				en: {
					CLOSE: "Закрыть"
				}
			}
		});
		
		// smooth scroll to anchor
		var $scrollToAnchor = $(".scroll-to-anchor");
		$scrollToAnchor.on("click", function (e) {
			e.preventDefault();
			$("html, body").animate({
				scrollTop: $($.attr(this, "href")).offset().top
			}, 400);
			return false;
		});
		
		// header banners slider
		var $headerBannersSlider = $("#headerBannersSlider");
		$headerBannersSlider.slick({
			dots: false,
			arrows: false,
			infinite: true,
			fade: true,
			speed: 1000,
			autoplay: true,
			autoplaySpeed: 6000
		});
		
		// close header banners
		var $closeHeadBanners = $("#closeHeadBanners");
		$closeHeadBanners.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.closest(".header-banners-slider-wrap").slideUp(200);
		});
		
		// header search
		var $headerSearch = $(".header-search");
		var $headerSearchInput = $(".header-search-form").find("input");
		var $headerSearchMatchesList = $(".header-search-matches__list");
		var $clearSearch = $headerSearch.find(".clear-search");
		var searchMatches = function() {
			var searchTextValue = $headerSearchInput.val();
			var options = {
				separateWordSearch: false
			};
			$headerSearchMatchesList.unmark({
				done: function() {
					$headerSearchMatchesList.mark(searchTextValue, options);
				}
			});
		};
		$headerSearchInput.on("input", function(){
			searchMatches();
			$headerSearch.addClass("opened-matches");
			$clearSearch.addClass("visible");
		});
		$(document).on("click", function (e) {
			if ( ( $(e.target).closest(".header-search").length )) return;
			$headerSearch.removeClass("opened-matches");
			e.stopPropagation();
		});
		
		// clear search
		$clearSearch.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.removeClass("visible");
			$headerSearch.removeClass("opened-matches");
			$this.closest(".header-search-form").find("input").val("").focus();
		});
		
		// show fixed cart
		var $fixedHeaderCart = $(".fixed-header-cart");
		function showFixedCart(){
			var bodyTopOffset = $(document).scrollTop();
			if (bodyTopOffset > 200) {
				$fixedHeaderCart.addClass("visible");
			} else {
				$fixedHeaderCart.removeClass("visible");
			}
		}
		showFixedCart();
		$(window).scroll(function () {
			showFixedCart();
		});
		
		// open placement dropdown
		var $openPlacementList = $("#openPlacementList");
		$openPlacementList.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.closest(".placement-box").toggleClass("opened");
		});
		$(document).on("click", function (e) {
			if ( ( $(e.target).closest(".placement-box").length )) return;
			$(".placement-box").removeClass("opened");
			e.stopPropagation();
		});
		
		// hover for main nav submenu
		var $headerMainNav = $(".header-main-nav");
		var $headerMainNavUL = $headerMainNav.find(".header-main-nav__box").find("> ul");
		var $headerMainNavItem = $headerMainNavUL.find("> li");
		$headerMainNavUL.hover(
			function() {
				$headerMainNavUL.addClass("hovered");
				setTimeout(function() {
					$headerMainNavUL.removeClass("hovered");
				}, 300);
			}, function() {
				$headerMainNavUL.removeClass("hovered");
			}
		);
		
		// close discount tooltip
		var $discountTooltip = $(".discount-tooltip");
		var $closeDiscountTooltip = $discountTooltip.find(".close-tooltip");
		$closeDiscountTooltip.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.closest($discountTooltip).addClass("hidden");
		});
		
		// open resp menu
		var $openRespMenu = $("#openRespMenu");
		var $fullResponsiveNav = $("#fullResponsiveNav");
		var $closeResponsiveNav = $("#closeRespNav");
		$openRespMenu.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$fullResponsiveNav.addClass("visible");
			$body.addClass("opened-resp-nav");
		});
		// close resp nav
		$closeResponsiveNav.on("click", function (e) {
			e.preventDefault();
			$fullResponsiveNav.removeClass("visible");
			$body.removeClass("opened-resp-nav");
			$(".slick-slider").slick('refresh');
		});
		
		// toggle resp submenu
		var $fullRespCatalogNav = $(".fr-catalog-nav");
		var $fullRespCatalogNavLI = $fullRespCatalogNav.find("li");
		var $FRhasSubmenuLink = $fullRespCatalogNav.find(".has-submenu").find("> a");
		var $backToFullNav = $("#backToFullNav");
		$FRhasSubmenuLink.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$fullRespCatalogNavLI.toggleClass("hidden");
			$backToFullNav.toggleClass("visible");
			$this.toggleClass("opened");
			$this.parent()
				.removeClass("hidden")
				.toggleClass("opened")
				.find(".submenu")
				.toggleClass("opened");
		});
		// back to full nav
		$backToFullNav.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.toggleClass("visible");
			$fullRespCatalogNavLI.removeClass("hidden opened").find(".submenu").removeClass("opened");
			$FRhasSubmenuLink.removeClass("opened");
		});
		
		// main banners slider
		var $mainBannersSlider = $("#mainBannersSlider");
		$mainBannersSlider.slick({
			centerMode: true,
			dots: true,
			arrows: false,
			speed: 200,
			infinite: true,
			centerPadding: '150px',
			slidesToShow: 1,
			autoplay: false,
			pauseOnHover: false,
			responsive: [
				{
					breakpoint: 767,
					settings: {
						centerPadding: '60px',
					}
				},
				{
					breakpoint: 480,
					settings: {
						centerPadding: '25px',
					}
				}
			]
		});
		
		// main responsive nav
		var $mainResponsiveNavHasSub = $(".main-responsive-nav .has-submenu > a");
		$mainResponsiveNavHasSub.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.parent().toggleClass("opened").find(".submenu").slideToggle(300);
		});
		
		// add to fav // add to cart
		var $addToFav = $(".add-to-fav");
		var $addToCart = $(".add-to-cart");
		$addToFav.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.toggleClass("added");
			/* ..... */
		});
		$addToCart.on("click", function (e) {
			// e.preventDefault();
			// var $this = $(this);
			// $this.toggleClass("added");
			/* ..... */
		});
		
		// main partners slider
		var $mainPartnersSlider = $("#mainPartnersSlider");
		$mainPartnersSlider.slick({
			dots: false,
			arrows: true,
			speed: 300,
			infinite: true,
			slidesToShow: 1,
			autoplay: false,
			adaptiveHeight: false,
			pauseOnHover: false,
			responsive: [
				{
					breakpoint: 767,
					settings: {
						centerMode: true,
						centerPadding: '20px',
					}
				}
			]
		});
		
		// view switcher
		var $viewSwitcherBtn = $(".view-switcher a");
		$viewSwitcherBtn.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$viewSwitcherBtn.removeClass("active");
			$this.addClass("active");
		});
		
		// filter dropdown open
		var $filterDropdownOpener = $(".filter-dropdown-opener");
		$filterDropdownOpener.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.closest(".filter-item").toggleClass("opened").siblings().removeClass("opened");
		});
		$(document).on("click", function (e) {
			if ( ( $(e.target).closest(".filter-item").length )) return;
			$(".filter-item").removeClass("opened");
			e.stopPropagation();
		});
		
		// filter shade
		var $filterShadesItem = $(".filter-shades-item");
		var $filterBtn = $(".filter-dropdown-btns .regular-btn");
		$filterShadesItem.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.toggleClass("selected");
			if($filterShadesItem.hasClass("selected")) {
				$this.closest(".filter-modal-form").find($filterBtn).removeAttr("disabled");
			} else {
				$this.closest(".filter-modal-form").find($filterBtn).attr("disabled", true);
			}
		});
		
		// filter sizes
		var $filterSizesItem = $(".filter-sizes-items .checkbox-label input");
		$filterSizesItem.change(function() {
			var $this = $(this);
			if(($this).is(":checked")) {
				$this.closest(".filter-modal-form").find($filterBtn).removeAttr("disabled");
			} else {
				$this.closest(".filter-modal-form").find($filterBtn).attr("disabled", true);
			}
		});
		
		
		// color search
		var $colorSearch = $(".catalog-color-search");
		var $colorSearchInput = $colorSearch.find("input");
		var $colorSearchMatchesList = $(".color-search-matches__list");
		var $clearColorSearch = $colorSearch.find(".clear-search");
		var colorSearchMatches = function() {
			var searchTextValue = $colorSearchInput.val();
			var options = {
				separateWordSearch: false
			};
			$colorSearchMatchesList.unmark({
				done: function() {
					$colorSearchMatchesList.mark(searchTextValue, options);
				}
			});
		};
		$colorSearchInput.on("input", function(){
			colorSearchMatches();
			$colorSearch.addClass("opened-matches");
			$clearColorSearch.addClass("visible");
		});
		$(document).on("click", function (e) {
			if ( ( $(e.target).closest(".catalog-color-search").length )) return;
			$colorSearch.removeClass("opened-matches");
			e.stopPropagation();
		});
		
		// clear search
		$clearColorSearch.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.removeClass("visible");
			$colorSearch.removeClass("opened-matches");
			$this.closest(".catalog-color-search").find("input").val("").focus();
		});
		
		
		// mobile filter
		var $openMobileFilter = $("#openMobileFilter");
		var $mobileFilter = $("#mobileFilter");
		var $closeMobileFilter = $("#closeMobileFilter");
		$openMobileFilter.on("click", function (e) {
			e.preventDefault();
			$mobileFilter.addClass("active");
			$body.addClass("opened-mobile-filter");
		});
		
		// close mobile filter
		$closeMobileFilter.on("click", function (e) {
			e.preventDefault();
			$mobileFilter.removeClass("active");
			$body.removeClass("opened-mobile-filter");
		});
		$(document).on("click", function (e) {
			if ( ( $(e.target).closest(".mobile-filter-box").length ) || ( $(e.target).closest($openMobileFilter).length ) || ( $(e.target).closest(".mobile-filter-modal").length ) ) return;
			$mobileFilter.removeClass("active");
			$body.removeClass("opened-mobile-filter");
			e.stopPropagation();
		});
		
		
		// similar goods slider
		var $similarGoodsSlider = $("#similarGoodsSlider");
		$similarGoodsSlider.slick({
			dots: false,
			arrows: true,
			speed: 300,
			infinite: true,
			slidesToShow: 4,
			autoplay: true,
			autoplaySpeed: 5000,
			pauseOnHover: true,
			responsive: [
				{
					breakpoint: 1485,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 991,
					settings: {
						slidesToShow: 2,
						centerMode: true,
						centerPadding: '70px',
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 1,
						centerMode: true,
						centerPadding: '75px',
					}
				}
			]
		});
		
		
		/*--- card sliders ---*/
		
		// card slider pics
		var $cardSliderPics = $(".card-slider-pics");
		$cardSliderPics.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			prevArrow: '<div class="prev-nav-arrow"><span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 115.7 29.1"><path d="M14.5 29.1L0 14.5 14.5 0l2.6 2.7L5.3 14.5l11.8 11.9-2.6 2.7z"/><path d="M2.7 12.6h113v3.8H2.7v-3.8z"/></svg></span></div>',
			nextArrow: '<div class="next-nav-arrow"><span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 115.7 29.1"><path d="M14.5 29.1L0 14.5 14.5 0l2.6 2.7L5.3 14.5l11.8 11.9-2.6 2.7z"/><path d="M2.7 12.6h113v3.8H2.7v-3.8z"/></svg></span></div>',
			dots: false,
			speed: 300,
			pauseOnHover: false,
			autoplay: false,
			asNavFor: '.card-slider-nav',
			responsive: [
				{
					breakpoint: 991,
					settings: {
						dots: true,
					}
				}
			]
		});
		
		// card slider nav
		var $cardSliderNav = $(".card-slider-nav");
		var card_slider_flag = true;
		var tmbs_counter = $cardSliderNav.find(".card-slide-nav-tmb").length;
		if(tmbs_counter <= 3){
			card_slider_flag = false;
		}
		$cardSliderNav.slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			asNavFor: '.card-slider-pics',
			dots: false,
			arrows: false,
			speed: 300,
			autoplay: false,
			pauseOnHover: false,
			focusOnSelect: true,
			centerMode: card_slider_flag,
			centerPadding: '0px'
		});
		
		
		// card sizes
		var $cardSizeItem = $(".card-size-item");
		$cardSizeItem.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$cardSizeItem.removeClass("selected");
			$this.addClass("selected");
			/* ..... */
		});
		
		/* quantity spinner */
		// more
		/*$body.on("click", ".qty-spinner__more", function (e) {
			e.preventDefault();
			var $this = $(this);
			var thisStep = $this.data("step");
			var thisPrice = $this.data("price");
			var $curValue = $this.parent().find(".qty-spinner__value span");
			var val = parseInt(($curValue).text());
			val += thisStep;
			$curValue.text(val);
		});
		// less
		$body.on("click", ".qty-spinner__less", function (e) {
			e.preventDefault();
			var $this = $(this);
			var thisStep = $this.data("step");
			var thisPrice = $this.data("price");
			var $curValue = $this.parent().find(".qty-spinner__value span");
			var val = parseInt(($curValue).text());
			if (val > 0)
				val -= thisStep;
			$curValue.text(val);
		});*/
		
		// card fast order
		var $openFastOrder = $(".open-fast-order");
		var $cardFastOrder = $(".card-form-fast-order");
		var $cardFastOrderTitle = $(".card-form-fast-order__title");
		$openFastOrder.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.closest(".card-form-btns").find($cardFastOrder).addClass("opened");
		});
		$cardFastOrderTitle.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.parent().removeClass("opened");
		});
		$(document).on("click", function (e) {
			if ( ( $(e.target).closest(".card-form-btns").length ) ) return;
			$cardFastOrder.removeClass("opened");
			e.stopPropagation();
		});
		
		// card desc switcher
		$body.on("click", ".card-seo-desc__title span", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.closest(".card-seo-desc").toggleClass("closed").find(".card-seo-desc__content").slideToggle(300);
		});
		
		// card packing switcher
		$body.on("click", ".card-packing-item .resp-switcher", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.closest(".card-packing-item").toggleClass("opened").find(".card-packing-item__content").slideToggle(300);
		});
		
		
		// open card popup
		var $openCardPopup = $(".open-card-popup");
		$openCardPopup.fancybox({
			animationEffect: "fade",
			touch: false,
			autoFocus: false,
			baseClass: "card-popup-fancybox",
			beforeLoad: function () {
				$(".popup-preloader-wrap").addClass("visible");
			},
			afterLoad: function () {
				setTimeout(function() {
					$(".popup-preloader-wrap").removeClass("visible");
				}, 500);
			},
			beforeShow: function () {
				$cardSliderPics.slick('refresh');
				$cardSliderNav.slick('refresh');
				$similarGoodsSlider.slick('refresh');
			},
			i18n: {
				en: {
					CLOSE: "Закрыть"
				}
			}
		});
		
		// open cart popup
		var $openCartPopup = $(".open-cart-popup");
		$openCartPopup.fancybox({
			animationEffect: "fade",
			touch: false,
			autoFocus: false,
			baseClass: "cart-popup-fancybox",
			beforeLoad: function () {
				$(".popup-preloader-wrap").addClass("visible");
			},
			afterLoad: function () {
				$( '#cartPopup .cart-container' ).load( '/personal/basket/ajax.php', function() {
					setTimeout(function() {
					$(".popup-preloader-wrap").removeClass("visible");
				}, 500);
				} );
			},
			i18n: {
				en: {
					CLOSE: "Закрыть"
				}
			}
		});
		
		
		/*-- checkout js --*/
		
		/* delivery */
		var $courierDeliveryAddress = $("#courierDeliveryAddress");
		var $deliveryRadio = $(".step-delivery .checkout-payment-items input:radio");
		$deliveryRadio.on("change", function() {
			if (this.value === "courier-delivery") {
				$courierDeliveryAddress.addClass("visible");
			} else {
				$courierDeliveryAddress.removeClass("visible");
			}
		});
		
		
		// lk more personals mobile
		var $showLKParams = $("#showLKParams");
		$showLKParams.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.slideUp(300).closest(".lk-personal-mobile").find(".hidden-user-params").slideToggle(300);
		});
		
		
		// thumbnails slider
		var $thumbnailsSlider = $(".thumbnails-slider");
		$thumbnailsSlider.each(function () {
			var $this = $(this);
			
			$this.slick({
				dots: false,
				arrows: false,
				infinite: false,
				speed: 400,
				slidesToShow: 1,
				autoplay: false,
				pauseOnHover: false,
				autoplaySpeed: 3000,
				responsive: [
					{
						breakpoint: 767,
						settings: {
							slidesToShow: 1,
							dots: true,
							centerMode: true,
							centerPadding: '20px',
							infinite: false,
						}
					}
				]
			});
			
			var $thisThumbnails = $this.closest(".thumbnails-slider-wrap").find(".thumbnails-slider-tmbs");
			var $thisThumbnail = $thisThumbnails.find("a");
			$thisThumbnail.on("click", function(e) {
				e.preventDefault();
				var $this = $(this);
				var thisSlideNumber = $this.data("slide");
				$thisThumbnail.removeClass("active");
				$this.addClass("active");
				$this.closest(".thumbnails-slider-wrap").find($thumbnailsSlider).slick('slickGoTo', thisSlideNumber - 1);
			});
			$this.on("beforeChange", function(event, slick, currentSlide, nextSlide){
				var slideIndex = nextSlide+1;
				$thisThumbnail.removeClass("active");
				$thisThumbnails.find("a[data-slide='"+ slideIndex +"']").addClass("active");
			});
			
			
		});
		
		
		// charity slider
		var $charitySlider = $("#charitySlider");
		$charitySlider.slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			dots: false,
			arrows: true,
			infinite: true,
			speed: 300,
			autoplaySpeed: 3000,
			autoplay: true,
			responsive: [
				{
					breakpoint: 991,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 1,
						centerMode: true,
						centerPadding: '20px',
						arrows: false,
						infinite: false
					}
				}
			]
		});
		
		
		// color selection
		var $colorSelectionShadeItem = $(".color-selection-shade-item").not(".select-all");
		var $colorSelectionShadeItemAll = $(".color-selection-shade-item.select-all");
		$colorSelectionShadeItem.on("click", function(e) {
			e.preventDefault();
			var $this = $(this);
			$this.toggleClass("selected");
			$this.closest(".color-selection-shades-list").find($colorSelectionShadeItemAll).removeClass("selected");
			if($this.hasClass("all")){
				$this.closest(".color-selection-shades-list").find($colorSelectionShadeItem).removeClass("selected all");
				$this.addClass("selected");
			}
		});
		$colorSelectionShadeItemAll.on("click", function(e) {
			e.preventDefault();
			var $this = $(this);
			$this.addClass("selected");
			$this.closest(".color-selection-shades-list").find($colorSelectionShadeItem).addClass("selected all");
		});
		
		
		// color selection checkboxes filter
		var $colorSelectionSearch = $(".color-selection-checkboxes-search");
		var $colorSelectionSearchInput = $(".color-selection-checkboxes-search input");
		var $colorSelectionSearchClear = $(".color-selection-checkboxes-search .clear-search");
		var $colorSelectionCheckboxes = $(".color-selection-checkboxes-box");
		var $colorSelectionCheckboxesItem = $(".color-selection-checkboxes-items .checkbox-box");
		var $colorSelectionCheckbox = $(".color-selection-checkboxes-box input").not(".check-all");
		var $colorSelectionCheckboxAll = $(".color-selection-checkboxes-box input.check-all");
		
		$colorSelectionCheckbox.on("change", function(e) {
			var $this = $(this);
			$this.closest($colorSelectionCheckboxes).find($colorSelectionCheckboxAll).prop("checked", false);
		});
		
		$colorSelectionCheckboxAll.on("click", function(e) {
			var $this = $(this);
			var $curCheckbox = $this.closest($colorSelectionCheckboxes).find($colorSelectionCheckbox);
			if($this.is(":checked")){
				$curCheckbox.prop("checked", true);
			} else {
				$curCheckbox.prop("checked", false);
			}
		});
		
		function searchColorsSelectionMatches(input){
			var $thisInput = input;
			var $thisItem = $thisInput.closest(".color-selection-checkboxes-wrap").find($colorSelectionCheckboxesItem);
			var searchTextValue = $thisInput.val();
			$thisItem.removeClass("hidden").unmark();
			if (searchTextValue) {
				$thisItem.mark(searchTextValue, {
					done: function() {
						$thisItem.not(":has(mark)").addClass("hidden");
					}
				});
			}
			
		}
		
		$colorSelectionSearchInput.on("input", function(){
			var $this = $(this);
			searchColorsSelectionMatches($this);
		});
		
		$colorSelectionSearchClear.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.removeClass("visible");
			$this.closest($colorSelectionSearch).find("input").val("").focus();
			$this.closest(".color-selection-checkboxes-wrap").find($colorSelectionCheckboxesItem).removeClass("hidden").unmark();
		});
		
		
		// close cart promocode msg
		var $cartPromocodeMsgClose = $(".cart-promocode-msg-close");
		$cartPromocodeMsgClose.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.parent().removeClass("visible");
		});

		// color search
		var $colorSelect = $(".color-select");
		var colorSelectPlaceholder = $colorSelect.data("placeholder");
		$colorSelect.SumoSelect({
			csvDispCount: 3,
			placeholder: colorSelectPlaceholder,
			search: true,
			searchText : 'Поиск по цветам',
			noMatch : 'Нет совпадений для "{0}"',
			// placeholder: placeholder,
			// captionFormat: placeholder + ' ({0})',
			captionFormatAllSelected: "",
			okCancelInMulti: false,
			locale:  ['Применить', 'Сбросить', 'Выбраны все']
		});
	
	});
	
})(window.jQuery);